class RPNCalculator

  def initialize
    @stack = []
  end

  def push num
    @stack << num
  end

  def plus
    perform_operation (:+)
  end

  def minus
    perform_operation (:-)
  end

  def divide
    perform_operation (:/)
  end

  def times
    perform_operation (:*)
  end

  def value
    @stack.last
  end

  def tokens (string)
    tokens = string.split
    tokens.map {|char| operation?(char) ? char.to_sym : char.to_i}
  end

  def evaluate (string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def operation? (char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end


  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.size < 2
    second = @stack.pop
    first = @stack.pop

    case op_symbol
    when :+
      @stack << first + second
    when :-
      @stack << first - second
    when :*
      @stack << first * second
    when :/
      @stack << first.fdiv(second)
    else
      @stack << first
      @stack << second
      raise "no such operand #{op_symbol}"
    end
  end
end
